#!/usr/bin/env bash

kafka-topics() {
    docker run --rm -it --net=host confluentinc/cp-kafka kafka-topics --zookeeper 127.0.0.1:2181 $@
}

kafkacat() {
    docker run --rm -it --net=host -v ${PWD}:/data -w /data registry.gitlab.com/kafka-devkit/kafkacat $@
}


# Simple stream
kafka-topics --create --partitions 4 --replication-factor 1 --topic input
kafka-topics --create --partitions 4 --replication-factor 1 --topic output

# Statefull stream
kafka-topics --create --partitions 4 --replication-factor 1 --topic jug-bill
kafka-topics --create --partitions 4 --replication-factor 1 --topic jug-user
kafka-topics --create --partitions 4 --replication-factor 1 --topic jug-user-bill
kafka-topics --create --partitions 4 --replication-factor 1 --topic jug-total-per-user


kafkacat -b localhost:9092 -P -t jug-user -K: -l users
kafkacat -b localhost:9092 -P -t jug-bill -K: -l bills