package com.cegedim.jug.simple.simple;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

@Slf4j
public class SimpleTopology {

    private final Topology topology;
    private final StreamsBuilder builder = new StreamsBuilder();

    public SimpleTopology() {

        // Defines topology
        createTopology(builder);

        // Build topology and print
        topology = builder.build();
        log.debug("\n{}", topology.describe().toString());
    }

    public Topology getTopology() {
        return topology;
    }

    private void createTopology(StreamsBuilder builder) {

        KStream<String, String> inputStream = builder.stream("input", Consumed.with(Serdes.String(), Serdes.String()));

        KStream<String, String> peekedStream = inputStream.peek((key, value) -> log.info("{}: {}", key, value));

        peekedStream.to("output", Produced.with(Serdes.String(), Serdes.String()));

    }

}
