package com.cegedim.jug.simple.simple;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * Create topics:
 * <p>
 * kafka-topics --zookeeper 127.0.0.1:2181 --create --partitions 4
 * --replication-factor 1 --topic input kafka-topics --zookeeper 127.0.0.1:2181
 * --create --partitions 4 --replication-factor 1 --topic output
 * <p>
 * <p>
 * Consume
 * <p>
 * kafkacat -b 127.0.0.1:9092 -C -t output
 * <p>
 * <p>
 * Produce
 * <p>
 * kafkacat -b 127.0.0.1:9092 -P -t input
 */
@Component
@Slf4j
public class SimpleStream implements SmartLifecycle {

    private final KafkaStreams kafkaStreams;

    public SimpleStream() {
        // 1. Configuration
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "simple-stream");
        streamsConfiguration.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE);
        streamsConfiguration.put(StreamsConfig.TOPOLOGY_OPTIMIZATION, StreamsConfig.OPTIMIZE);
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // 2. Topology
        SimpleTopology topology = new SimpleTopology();

        // 3. Instantiate stream processors
        kafkaStreams = new KafkaStreams(topology.getTopology(), streamsConfiguration);
    }

    @Override
    public void start() {
        kafkaStreams.start();
    }

    @Override
    public void stop() {
        kafkaStreams.close();
    }

    @Override
    public boolean isRunning() {
        return kafkaStreams.state().isRunning();
    }
}
