package com.cegedim.jug.simple.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBill {
    private String billId;
    private String userId;
    private String userName;

    public UserBill(Bill bill, User user) {
        this.billId = bill.getId();
        this.userId = user.getId();
        this.userName = user.getName();
    }
}
