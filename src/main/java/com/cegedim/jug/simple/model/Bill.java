package com.cegedim.jug.simple.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bill {
    private String id;
    private String userId;
    private Header header = new Header();
    private List<Line> lines = new ArrayList<>();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Line {
        private String item;
        private BigDecimal price;
    }

    @Data
    public static class Header {
        private LocalDateTime date;
        private String enterprise;
    }
}
