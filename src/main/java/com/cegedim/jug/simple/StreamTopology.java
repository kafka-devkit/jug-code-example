package com.cegedim.jug.simple;

import org.apache.kafka.streams.Topology;

public interface StreamTopology {

    Topology getTopology();
}
