package com.cegedim.jug.simple.statefull;

import com.cegedim.jug.simple.StreamTopology;
import com.cegedim.jug.simple.model.Bill;
import com.cegedim.jug.simple.model.User;
import com.cegedim.jug.simple.model.UserBill;
import com.cegedim.jug.simple.serdes.BigDecimalSerde;
import com.cegedim.jug.simple.serdes.BillSerde;
import com.cegedim.jug.simple.serdes.UserBillSerde;
import com.cegedim.jug.simple.serdes.UserSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;

import java.math.BigDecimal;

@Slf4j
public class StateFullStreamTopology implements StreamTopology {

    private final Topology topology;
    private final StreamsBuilder builder = new StreamsBuilder();

    public StateFullStreamTopology() {

        // Defines topology
        createTopology(builder);

        // Build topology and print
        topology = builder.build();
        log.debug("\n{}", topology.describe().toString());
    }

    public Topology getTopology() {
        return topology;
    }

    private void createTopology(StreamsBuilder builder) {

        // Create a user table
        KTable<String, User> userKTable = builder.table(Constants.USER_TOPIC, Materialized
                .<String, User, KeyValueStore<Bytes, byte[]>>as(Constants.USER_STORE)
                .withKeySerde(Serdes.String())
                .withValueSerde(new UserSerde()));
        //userKTable.toStream().peek((key, value) -> log.debug("userKTable {}: {}", key, value));

        // Join bills with users
        KStream<String, Bill> billStream = builder.stream(Constants.BILL_TOPIC, Consumed.with(Serdes.String(), new BillSerde()));

        // Here we force the repartition of data. The key is userId --> all data
        // with the same key goes to the same partition
        KStream<String, Bill> billStreamWithUserIdKey = billStream
//                .peek((key, value) -> log.debug("billStream {}: {}", key, value))
                .selectKey((key, value) -> value.getUserId());

        KStream<String, UserBill> joinedBillAndUser = billStreamWithUserIdKey
                .join(userKTable, (bill, user) -> {
                    log.debug("Joining data between {} and {}", bill, user);
                    return new UserBill(bill, user);
                }, Joined
                        .with(Serdes.String(), new BillSerde(), new UserSerde()));

        joinedBillAndUser
//                .peek((key, value) -> log.debug("joinedBillAndUser {}: {}", key, value))
                .to(Constants.USER_BILL_TOPIC, Produced.with(Serdes.String(), new UserBillSerde()));

        computeTotalPrice(billStreamWithUserIdKey);

    }

    private void computeTotalPrice(KStream<String, Bill> billStreamWithUserIdKey) {
        // Extracting / Transforming and aggregating data by summing all prices
        // by userId.

        KTable<String, BigDecimal> aggregatedAmountByUser = billStreamWithUserIdKey
                .flatMapValues(Bill::getLines)
                .mapValues(Bill.Line::getPrice)
                .groupByKey(Serialized.with(Serdes.String(), new BigDecimalSerde()))
                .aggregate(
                        () -> BigDecimal.ZERO,
                        (key, value, aggregatedTotalAmount) ->
                                aggregatedTotalAmount.add(value)
                        ,
                        Materialized
                                .<String, BigDecimal, KeyValueStore<Bytes, byte[]>>as(Constants.TOTAL_PER_USER_STORE)
                                .withKeySerde(new Serdes.StringSerde())
                                .withValueSerde(new BigDecimalSerde()));

        // Producing data to an external topic.
        aggregatedAmountByUser.toStream().to(Constants.TOTAL_PER_USER, Produced.with(Serdes.String(), new BigDecimalSerde()));
    }

}
