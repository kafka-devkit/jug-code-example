package com.cegedim.jug.simple.statefull;

import com.cegedim.jug.simple.StreamTopology;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * Create topics:
 * <p>
 * kafka-topics --zookeeper 127.0.0.1:2181 --create --partitions 4 --replication-factor 1 --topic jug-bill
 * kafka-topics --zookeeper 127.0.0.1:2181 --create --partitions 4 --replication-factor 1 --topic jug-user
 * kafka-topics --zookeeper 127.0.0.1:2181 --create --partitions 4 --replication-factor 1 --topic jug-user-bill
 * kafka-topics --zookeeper 127.0.0.1:2181 --create --partitions 4 --replication-factor 1 --topic jug-total-per-user
 * <p>
 * <p>
 * Consume
 * <p>
 * kafkacat -b 127.0.0.1:9092 -C -t jug-bill kafkacat -b 127.0.0.1:9092 -C -t jug-user
 * kafkacat -b 127.0.0.1:9092 -C -t jug-user-bill
 * <p>
 * <p>
 * Produce
 * <p>
 * kafkacat -b 127.0.0.1:9092 -P -t user kafkacat -b 127.0.0.1:9092 -P -t jug-bill
 *
 * <p>
 * <p> Some useful commands</p>
 * kafka-topics --zookeeper localhost --delete --topic jug-user
 * delete all jug.* topics
 * kafka-topics --zookeeper localhost --delete --topic jug.*
 * kafka-topics --zookeeper localhost --list
 * <p>
 * <p>
 * insert a list of data
 * kafkacat -b localhost:9092 -P -t jug-user -l users
 * kafkacat -b localhost:9092 -P -t jug-bill -l bills
 * <p>
 * read with key
 * kafkacat -b localhost:9092 -C -t jug-total-per-user -K":"
 */
@Component
@Slf4j
public class StateFullStream implements SmartLifecycle {

    private final KafkaStreams kafkaStreams;


    public StateFullStream(@Value("${spring.cloud.client.ip-address}") String ipAddress, @Value("${server.port}") int serverPort) {

        // 1. Configuration
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "state-full-stream");
        streamsConfiguration.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE);
        streamsConfiguration.put(
                StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                LogAndContinueExceptionHandler.class);
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        //streamsConfiguration.put(StreamsConfig.APPLICATION_SERVER_CONFIG, String.format("%s:%s", ipAddress, serverPort));

        // 2. Topology
        StreamTopology topology = new StateFullStreamTopology();

        // 3. Instantiate stream processors
        kafkaStreams = new KafkaStreams(topology.getTopology(), streamsConfiguration);
    }


    @Override
    public void start() {
        kafkaStreams.start();
    }

    @Override
    public void stop() {
        kafkaStreams.close();
    }

    @Override
    public boolean isRunning() {
        return kafkaStreams.state().isRunning();
    }

    public KafkaStreams getKafkaStreams() {
        return kafkaStreams;
    }

}
