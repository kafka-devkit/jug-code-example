package com.cegedim.jug.simple.statefull;

import com.cegedim.jug.simple.model.Bill;
import com.cegedim.jug.simple.model.User;
import com.cegedim.jug.simple.serdes.BillSerde;
import com.cegedim.jug.simple.serdes.UserSerde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.Joined;

public class Constants {

    public static final String BILL_TOPIC = "jug-bill";
    public static final String USER_TOPIC = "jug-user";
    public static final String USER_BILL_TOPIC = "jug-user-bill";
    public static final String USER_STORE = "jug-user_store";
    public static final String TOTAL_PER_USER = "jug-total-per-user";
    public static final String TOTAL_PER_USER_STORE = "jug-total-per-user-store";


    private Constants() {
    }
}
