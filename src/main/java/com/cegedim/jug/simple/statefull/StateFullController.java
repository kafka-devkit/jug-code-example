package com.cegedim.jug.simple.statefull;

import com.cegedim.jug.simple.model.User;
import com.cegedim.jug.simple.statefull.interactive.query.HostStoreInfo;
import com.cegedim.jug.simple.statefull.interactive.query.MetadataService;
import com.cegedim.jug.simple.statefull.interactive.query.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.net.URI;

@Component
@Slf4j
@RestController
@RequestMapping("/v1")
public class StateFullController {


    public static final StringSerializer STRING_SERIALIZER = new StringSerializer();

    private final MetadataService metadataService;
    private final RestTemplate restTemplate = new RestTemplate();
    private final StateFullStream stream;

    public StateFullController(StateFullStream stream, @Value("${spring.cloud.client.ip-address}") String host, @Value("${server.port}") int port) {
        this.stream = stream;
        this.metadataService = new MetadataService(stream.getKafkaStreams(), host, port);
    }

    @GetMapping(value = "/users/{key}", produces = "application/json")
    public User getUserBill(@NotNull @PathVariable String key) {
        HostStoreInfo hostStoreInfo =
                metadataService.streamsMetadataForStoreAndKey(Constants.USER_STORE, key, STRING_SERIALIZER);

        // If data are not stored on this node, we query other node
        // When node registers to consumer group it gives its IP and PORT in APPLICATION_SERVER_CONFIG
        if (!metadataService.thisHost(hostStoreInfo)) {
            return restTemplate.getForObject(
                    URI.create(
                            String.format(
                                    "http://%s:%d/v1/users/%s",
                                    hostStoreInfo.getHost(),
                                    hostStoreInfo.getPort(),
                                    key)),
                    User.class);
        }

        // Lookup the KeyValueStore with the provided storeName
        final ReadOnlyKeyValueStore<String, User> store = stream.getKafkaStreams()
                .store(Constants.USER_STORE, QueryableStoreTypes.keyValueStore());

        // Get the value from the store
        final User value = store.get(key);
        if (value == null) {
            throw new ResourceNotFoundException();
        }
        return value;
    }

}
