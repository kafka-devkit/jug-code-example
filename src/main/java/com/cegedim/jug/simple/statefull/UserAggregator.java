package com.cegedim.jug.simple.statefull;

import com.cegedim.jug.simple.model.User;
import org.apache.kafka.streams.kstream.Aggregator;

public class UserAggregator implements Aggregator<String, User, User> {

    @Override
    public User apply(String key, User value, User aggregate) {
        return value;
    }
}
