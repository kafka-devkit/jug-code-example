package com.cegedim.jug.simple.serdes;

import com.cegedim.jug.simple.model.Bill;
import com.cegedim.jug.simple.serdes.utils.JsonDeserializer;
import com.cegedim.jug.simple.serdes.utils.JsonSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.kafka.common.serialization.Serdes;

public class BillSerde extends Serdes.WrapperSerde<Bill> {
    public BillSerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(new TypeReference<Bill>() {
        }));
    }
}
