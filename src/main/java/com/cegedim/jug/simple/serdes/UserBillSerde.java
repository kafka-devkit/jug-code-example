package com.cegedim.jug.simple.serdes;

import com.cegedim.jug.simple.model.UserBill;
import com.cegedim.jug.simple.serdes.utils.JsonDeserializer;
import com.cegedim.jug.simple.serdes.utils.JsonSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.kafka.common.serialization.Serdes;

public class UserBillSerde extends Serdes.WrapperSerde<UserBill> {
    public UserBillSerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(new TypeReference<UserBill>() {
        }));
    }
}
