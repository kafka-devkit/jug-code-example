package com.cegedim.jug.simple.serdes.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class JsonDeserializer<T> implements Deserializer<T> {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TypeReference<T> typeReference;

    public JsonDeserializer(TypeReference<T> typeReference) {
        this.typeReference = typeReference;
    }

    @Override
    public void configure(Map<String, ?> props, boolean isKey) {
        // No implementation
    }

    @Override
    public T deserialize(String topic, byte[] bytes) {
        if (bytes == null || bytes.length == 0)
            return null;

        T data;
        try {
            data = objectMapper.readValue(bytes, typeReference);
        } catch (Exception e) {
            throw new SerializationException(e);
        }

        return data;
    }

    @Override
    public void close() {
        // No implementation
    }
}
