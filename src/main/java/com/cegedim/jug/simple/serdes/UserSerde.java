package com.cegedim.jug.simple.serdes;

import com.cegedim.jug.simple.model.User;
import com.cegedim.jug.simple.serdes.utils.JsonDeserializer;
import com.cegedim.jug.simple.serdes.utils.JsonSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.kafka.common.serialization.Serdes;

public class UserSerde extends Serdes.WrapperSerde<User> {
    public UserSerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(new TypeReference<User>() {
        }));
    }
}
