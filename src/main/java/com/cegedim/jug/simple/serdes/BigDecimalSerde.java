package com.cegedim.jug.simple.serdes;

import com.cegedim.jug.simple.serdes.utils.JsonDeserializer;
import com.cegedim.jug.simple.serdes.utils.JsonSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.kafka.common.serialization.Serdes;

import java.math.BigDecimal;

public class BigDecimalSerde extends Serdes.WrapperSerde<BigDecimal> {
    public BigDecimalSerde() {
        super(new JsonSerializer<>(), new JsonDeserializer<>(new TypeReference<BigDecimal>() {
        }));
    }
}
