package com.cegedim;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"junit:build/test-results/TEST-Cucumber.xml"}, tags = {})
public class RunCucumberTest {

}