package com.cegedim.jug.simple.simple;

import com.cegedim.jug.simple.model.Bill;
import com.cegedim.jug.simple.model.User;
import com.cegedim.jug.simple.model.UserBill;
import com.cegedim.jug.simple.serdes.utils.JsonDeserializer;
import com.cegedim.jug.simple.serdes.utils.JsonSerializer;
import com.cegedim.jug.simple.statefull.StateFullStreamTopology;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.kafka.streams.test.OutputVerifier;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import static com.cegedim.jug.simple.statefull.Constants.*;

@Slf4j
public class StateFullTopologyTest {

    public static final JsonDeserializer<UserBill> USER_BILL_JSON_DESERIALIZER = new JsonDeserializer<>(new TypeReference<UserBill>() {
    });
    private StateFullStreamTopology target;
    private TopologyTestDriver testDriver;

    private String bill1Id;
    private String bill2Id;
    private String user1Id;

    @Before
    public void setUp() throws Exception {
        target = new StateFullStreamTopology();

        // setup test driver
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        testDriver = new TopologyTestDriver(target.getTopology(), config);

        // Users
        user1Id = UUID.randomUUID().toString();

        // Bills
        bill1Id = UUID.randomUUID().toString();
        bill2Id = UUID.randomUUID().toString();

    }

    @After
    public void tearDown() throws Exception {
        testDriver.close();
    }

    @Test
    public void basicTest() {
        // User creation
        ConsumerRecordFactory<String, User> userFactory = new ConsumerRecordFactory<>(USER_TOPIC, new StringSerializer(), new JsonSerializer<>());
        User user1 = new User(user1Id, "Gionco");
        testDriver.pipeInput(userFactory.create(USER_TOPIC, user1Id, user1, 42L));

        // Bill creation
        ConsumerRecordFactory<String, Bill> billFactory = new ConsumerRecordFactory<>(BILL_TOPIC, new StringSerializer(), new JsonSerializer<>());
        Bill bill1 = getBillWithTwoLines(user1Id, bill1Id);
        Bill bill2 = getBillWithOneLine(user1Id, bill1Id);
        testDriver.pipeInput(billFactory.create(BILL_TOPIC, bill1Id, bill1, 42L));
        testDriver.pipeInput(billFactory.create(BILL_TOPIC, bill2Id, bill2, 42L));


        // Check the projection of data UserBill
        ProducerRecord<String, UserBill> userBillRecord;
        userBillRecord = testDriver.readOutput(USER_BILL_TOPIC, new StringDeserializer(), USER_BILL_JSON_DESERIALIZER);
        OutputVerifier.compareValue(userBillRecord, new UserBill(bill1Id, user1Id, "Gionco"));

        // Check sum in Ktable
        KeyValueStore<String, BigDecimal> store = testDriver.getKeyValueStore(TOTAL_PER_USER_STORE);
        Assert.assertEquals("Data should be aggregated ans sumed", BigDecimal.valueOf(190.0), store.get(user1Id));
    }

    private Bill getBillWithOneLine(String user1Id, String bill1Id) {
        List<Bill.Line> lines2 = new ArrayList<>();
        lines2.add(new Bill.Line("hat", BigDecimal.valueOf(90.0)));
        return new Bill(bill1Id, user1Id, null, lines2);
    }

    private Bill getBillWithTwoLines(String user1Id, String bill1Id) {
        List<Bill.Line> lines1 = new ArrayList<>();
        lines1.add(new Bill.Line("shoes", BigDecimal.valueOf(60.0)));
        lines1.add(new Bill.Line("watch", BigDecimal.valueOf(40.0)));
        return new Bill(bill1Id, user1Id, null, lines1);
    }
}