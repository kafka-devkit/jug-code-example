package com.cegedim.jug.simple.simple;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.kafka.streams.test.OutputVerifier;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

public class SimpleTopologyTest {

    private SimpleTopology target;
    private TopologyTestDriver testDriver;

    @Before
    public void setUp() throws Exception {
        target = new SimpleTopology();

        // setup test driver
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        testDriver = new TopologyTestDriver(target.getTopology(), config);
    }

    @After
    public void tearDown() throws Exception {
        testDriver.close();
    }

    @Test
    public void simpleTest() {
        ConsumerRecordFactory<String, String> factory = new ConsumerRecordFactory<>("input", new StringSerializer(), new StringSerializer());
        testDriver.pipeInput(factory.create("input", "key", "value", 42L));
        testDriver.pipeInput(factory.create("input", "key1", "value1", 42L));


        ProducerRecord<String, String> outputRecord;

        outputRecord = testDriver.readOutput("output", new StringDeserializer(), new StringDeserializer());
        OutputVerifier.compareKeyValue(outputRecord, "key", "value");

        outputRecord = testDriver.readOutput("output", new StringDeserializer(), new StringDeserializer());
        OutputVerifier.compareKeyValue(outputRecord, "key1", "value1");
    }
}