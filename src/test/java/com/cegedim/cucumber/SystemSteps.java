package com.cegedim.cucumber;

import com.cegedim.jug.simple.StreamTopology;
import com.cegedim.jug.simple.model.UserBill;
import com.cegedim.jug.simple.serdes.utils.JsonDeserializer;
import com.cegedim.jug.simple.statefull.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.cucumber.datatable.DataTable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.kafka.streams.test.OutputVerifier;
import org.junit.Assert;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Stream;


@Slf4j
public class SystemSteps {

    public static final JsonDeserializer<UserBill> USER_BILL_JSON_DESERIALIZER = new JsonDeserializer<>(new TypeReference<UserBill>() {
    });
    private static final String ID_OF = "id of ";
    private static final String IS_EMPTY = "is empty";
    private static final String IS_NOT_EMPTY = "is not empty";
    private static final String[] STRING_ARRAY = new String[0];
    private static final long DEFAULT_TIMEOUT_MILISECONDS = 100;
    private SystemState state;

    @Before
    public void setUp() {
        state = new SystemState();
    }

    @After
    public void tearDown() {
        state.getTopologyDriver().close();
    }


    @And("^the system knows about a valid \"([^\"]*)\" named \"([^\"]*)\"$")
    public void theSystemKnowsAboutAValidNamed(String type, String name) {
        state.addId(name, UUID.randomUUID().toString());
    }

    @And("^the system knows about a valid list of \"([^\"]*)\" named \"([^\"]*)\"$")
    public Map<String, Object> theSystemKnowsAboutAValidListOfNamed(String type, String packName, DataTable table) {
        Map<String, Object> stateItem = new HashMap<>();
        List<Object> stateItemList = new ArrayList<>();

        table.asMaps().forEach(line -> {
            Object value = state.getObjectByType(type);
            JXPathContext ctx = JXPathContext.newContext(value);

            for (Map.Entry<String, String> item : line.entrySet()) {
                ctx = handleItem(item, ctx);
            }

            stateItemList.add(ctx.getValue("/"));
            if (line.containsKey("key")) {
                String keyValue = line.get("key");
                if (keyValue.startsWith(ID_OF)) {
                    keyValue = state.getId(keyValue.substring(ID_OF.length()));
                }
                stateItem.put(keyValue, ctx.getValue("/"));
            } else {
                stateItem.put(UUID.randomUUID().toString(), ctx.getValue("/"));
            }
        });
        state.putDataTable(packName, table);
        state.putList(packName, stateItemList);
        state.putMap(packName, stateItem);
        return stateItem;
    }

    @And("^the system knows about a valid list of \"([^\"]*)\" named \"([^\"]*)\" in topic \"([^\"]*)\"$")
    public void theSystemKnowsAboutAValidListOfNamedInTopic(String type, String packName, String topicName,
                                                            DataTable table) {

        theSystemKnowsAboutAValidListOfNamed(type, packName, table).entrySet().forEach(entry -> {
            try {
                send(topicName, state.getObjectMapper().writeValueAsString(entry.getValue()), entry.getKey());
            } catch (JsonProcessingException e) {
                log.error(e.getMessage(), e);
                Assert.fail(e.getMessage());
            }
        });

    }


    private void checkObject(Object target, Map<String, String> table) {
        final JXPathContext ctx = JXPathContext.newContext(target);
        table.forEach((path, value) -> {
            if ("key".equals(path)) {
                log.debug("Skip key column");
            } else if (ctx.getValue(path) instanceof List) {
                final List subtarget = (List) ctx.getValue(path);
                if (value.contains("contains ")) {

                    value = value.substring("contains ".length());
                    Stream.of(value.split(",")).map(String::trim).forEach(s -> {
                        Assert.assertTrue(
                                String.format("%s with value %s expected to contain %s", path, subtarget, s),
                                subtarget.contains(s));
                    });
                } else {
                    checkList(subtarget, state.getDataTable(value), null);
                }
            } else if (ctx.getValue(path) instanceof Map) {
                checkMap((Map) ctx.getValue(path), state.getDataTable(value));
            } else if (ctx.getValue(path) instanceof BigDecimal) {
                final BigDecimal expected = new BigDecimal(value);
                final BigDecimal actual = (BigDecimal) ctx.getValue(path);
                Assert.assertEquals(
                        String.format("%s with value %s expected but had %s", path, expected, actual),
                        expected.setScale(2, RoundingMode.DOWN),
                        actual.setScale(2, RoundingMode.DOWN));
            } else {
                if (value.startsWith(ID_OF)) {
                    value = state.getId(value.substring(ID_OF.length()));
                }
                if (value.equals(IS_EMPTY)) {
                    Assert.assertTrue(String.format("%s should be empty or null", path), isEmpty(ctx.getValue(path)));
                } else if (value.equals(IS_NOT_EMPTY)) {
                    Assert.assertFalse(
                            String.format("%s should not be empty or null", path),
                            isEmpty(ctx.getValue(path)));
                } else {
                    Assert.assertEquals(
                            String.format(
                                    "%s with value %s expected but had %s",
                                    path,
                                    value,
                                    ctx.getValue(path).toString()),
                            value,
                            ctx.getValue(path).toString());
                }
            }
        });

    }

    private boolean isEmpty(Object value) {
        if (value == null) {
            return true;
        }
        if (value instanceof String) {
            return ((String) value).isEmpty();
        }
        return false;
    }

    private void checkMap(Map target, DataTable table) {
        table.asMaps().forEach(m -> {
            Object targetValue = target.get(m.get("key"));
            Assert.assertNotNull("missing item " + m.get("key") + " in map", targetValue);
            checkObject(targetValue, m);
        });
    }

    private void checkList(List target, DataTable table, String sortPath) {
        Assert.assertEquals("Check should be done on lists with same sizes", table.asMaps().size(), target.size());

        List<Map<String, String>> maps = new ArrayList<>(table.asMaps());
        if (sortPath != null) {
            target.sort((o1, o2) -> {
                Object ctx1 = JXPathContext.newContext(o1).getValue(sortPath);
                Object ctx2 = JXPathContext.newContext(o2).getValue(sortPath);
                // if(ctx1 instanceof Comparable) {
                // return ((Comparable) ctx1).compareTo((Comparable)ctx2);
                // } else {
                // return ctx1.toString().compareTo(ctx2.toString());
                // }
                return ctx1.toString().compareTo(ctx2.toString());
            });

            maps.sort((o1, o2) -> {
                String value1 = o1.get(sortPath);
                String value2 = o2.get(sortPath);
                if (value1.startsWith(ID_OF)) {
                    value1 = state.getId(value1.substring(ID_OF.length()));
                }
                if (value2.startsWith(ID_OF)) {
                    value2 = state.getId(value2.substring(ID_OF.length()));
                }
                return value1.compareTo(value2);
            });

        }

        for (int i = 0; i < target.size(); i++) {
            checkObject(target.get(i), maps.get(i));
        }

    }


    private JXPathContext handleItem(Map.Entry<String, String> item, JXPathContext ctx) {
        String key = item.getKey();
        String value = item.getValue();

        if (value != null && !"key".equals(key)) {

            Object node = ctx.getPointer(key).getNode();

            if (node instanceof List) {
                Stream.of(value.split(",")).map(String::trim).forEach(valueKey -> {
                    Assert.assertNotNull(
                            valueKey + " list is null but should be provided in gherkin",
                            state.getList(valueKey));
                    ((List) node).addAll(state.getList(valueKey)); // NOSONAR
                });

            } else if (node instanceof Map) {
                Stream.of(value.split(",")).map(String::trim).forEach(valueKey -> {
                    Assert.assertNotNull(
                            valueKey + " map is null but should be provided in gherkin",
                            state.getList(valueKey));
                    ((Map) node).putAll(state.getMap(valueKey)); // NOSONAR
                });
            } else if (node instanceof Enum) {
                Enum val = Enum.valueOf((Class<Enum>) node.getClass(), value);
                ctx.setValue(key, val);
            } else if (value.startsWith(ID_OF)) {
                String id = state.getId(value.substring(ID_OF.length()));
                Assert.assertNotNull("Id of " + value + " is null but should be provided in gherkin", id);
                ctx.setValue(key, id);
            } else if ("/".equals(key)) {
                return JXPathContext.newContext(value);
            } else {
                ctx.setValue(key, value);
            }
        }
        return ctx;
    }

    private void send(String inputTopic, String object, String key) {

        TopologyTestDriver driver = state.getTopologyDriver();

        ConsumerRecordFactory<String, String> factory = new ConsumerRecordFactory<>(inputTopic, new StringSerializer(), new StringSerializer());

        driver.pipeInput(factory.create(inputTopic, key, object));
    }


    @Given("topology {string}")
    public void topology(String name) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<StreamTopology> clazz = (Class<StreamTopology>) Class.forName(name);
        StreamTopology streamTopology = clazz.newInstance();

        // setup test driver
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        config.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE);
        TopologyTestDriver testDriver = new TopologyTestDriver(streamTopology.getTopology(), config);

        state.setTopologyDriver(testDriver);
    }

    @And("I send those {string} in topic {string}")
    public void iSendThoseInTopic(String type, String topicName, DataTable table) throws InterruptedException {
        theSystemKnowsAboutAValidListOfNamedInTopic(type, "iSendThoseInTopic", topicName, table);
    }

    @Then("Total bill sum of {string} is equal to {double}")
    public void totalBillSumOfIsEqualTo(String user, Double price) {
        TopologyTestDriver testDriver = state.getTopologyDriver();

        KeyValueStore<String, BigDecimal> store = testDriver.getKeyValueStore(Constants.TOTAL_PER_USER_STORE);
        Assert.assertEquals("Data should be aggregated ans sumed", BigDecimal.valueOf(price), store.get(state.getId(user)));
    }

    @And("a bill {string} has bean joined with user {string}")
    public void aBillHasBeanJoinedWithUser(String bill, String user) {
        for (ProducerRecord<String, UserBill> result : state.getResults()) {
            if (state.getId(bill).equals(result.value().getBillId()) && state.getId(user).equals(result.value().getUserId())) {
                OutputVerifier.compareKeyValue(result, state.getId(user), new UserBill(state.getId(bill), state.getId(user), user));
                return;
            }
        }
    }

    @And("I read {int} times in {string} topic")
    public void iReadTimesInTopic(int num, String topicName) {
        TopologyTestDriver testDriver = state.getTopologyDriver();
        ProducerRecord<String, UserBill> userBillRecord;
        for (int i = 0; i < num; i++) {
            userBillRecord = testDriver.readOutput(topicName, new StringDeserializer(), USER_BILL_JSON_DESERIALIZER);
            state.addResult(userBillRecord);
        }
    }
}
