package com.cegedim.cucumber;

import com.cegedim.jug.simple.model.Bill;
import com.cegedim.jug.simple.model.User;
import com.cegedim.jug.simple.model.UserBill;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cucumber.datatable.DataTable;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.streams.TopologyTestDriver;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
public class SystemState {
    private final ObjectMapper objectMapper;
    private final Map<String, DataTable> dataTables = new ConcurrentHashMap<>();
    private final Map<String, Collection<Object>> listOfObjects = new ConcurrentHashMap<>();
    private final Map<String, Map<String, Object>> mapOfObjects = new ConcurrentHashMap<>();
    private final Map<String, String> ids = new ConcurrentHashMap<>();
    private final List<ProducerRecord<String, UserBill>> results = new CopyOnWriteArrayList<ProducerRecord<String, UserBill>>();
    private TopologyTestDriver testDriver;

    public SystemState() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public void putList(String key, Collection<Object> items) {
        listOfObjects.put(key, items);
    }

    public Collection<Object> getList(String key) {
        return listOfObjects.get(key);
    }

    public void addId(String name, String value) {
        ids.put(name, value);
    }

    public String getId(String name) {
        return ids.get(name);
    }

    public Object getObjectByType(String type) {
        switch (type) {
            case "string":
                return "";
            case "map":
                return new HashMap<>();
            case "list":
                return new ArrayList<>();
            case "line":
            case "lines":
                return new Bill.Line();
            case "user":
            case "users":
                return new User();
            case "bill":
            case "bills":
                return new Bill();
            case "userBill":
            case "userBills":
                return new UserBill();
            default:
                return null;
        }
    }

    public DataTable getDataTable(String key) {
        return dataTables.get(key);
    }

    public void putDataTable(String key, DataTable value) {
        dataTables.put(key, value);
    }

    public Map<String, Object> getMap(String key) {
        return mapOfObjects.get(key);
    }

    public Map putMap(String key, Map<String, Object> value) {
        return mapOfObjects.put(key, value);
    }

    public void addResult(ProducerRecord<String, UserBill> record) {
        results.add(record);
    }

    public List<ProducerRecord<String, UserBill>> getResults() {
        return results;
    }

    public TopologyTestDriver getTopologyDriver() {
        return testDriver;
    }

    public void setTopologyDriver(TopologyTestDriver testDriver) {
        this.testDriver = testDriver;
    }
}
