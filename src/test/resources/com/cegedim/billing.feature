Feature: Bills statistics

  Bills total per user should


  Background:
    Given topology "com.cegedim.jug.simple.statefull.StateFullStreamTopology"
    And the system knows about a valid "user" named "John"
    And the system knows about a valid "bill" named "One"
    And the system knows about a valid "bill" named "Two"
    And I send those "users" in topic "jug-user"
      | key        | /id        | /name |
      | id of John | id of John | John  |


  Scenario: Check sum of bills

    Given the system knows about a valid list of "lines" named "Lines of bill one"
      | /item | /price |
      | shoes | 60.0   |
      | watch | 40.0   |
    And the system knows about a valid list of "lines" named "Lines of bill two"
      | /item | /price |
      | hat   | 90.0   |
    When I send those "bills" in topic "jug-bill"
      | /id       | /userId    | /lines            |
      | id of One | id of John | Lines of bill one |
      | id of Two | id of John | Lines of bill two |
    Then Total bill sum of "John" is equal to 190.0

  Scenario: Check output

    Given the system knows about a valid list of "lines" named "Lines of bill one"
      | /item | /price |
      | shoes | 60.0   |
      | watch | 40.0   |
    And the system knows about a valid list of "lines" named "Lines of bill two"
      | /item | /price |
      | hat   | 90.0   |
    When I send those "bills" in topic "jug-bill"
      | /id       | /userId    | /lines            |
      | id of One | id of John | Lines of bill one |
      | id of Two | id of John | Lines of bill two |
    And I read 2 times in "jug-user-bill" topic
    Then a bill "One" has bean joined with user "John"


